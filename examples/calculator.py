from sspm.pluginmanager import PluginManager
from pathlib import Path

if __name__ == "__main__":
    path = Path("./plugins").resolve().as_posix()
    plugin_manager = PluginManager("./plugins")
    plugin_manager.import_plugins()

    # To see all loaded plugins you can use active_plugins
    print("Showing Loaded Plugins:")
    for plugin_name in plugin_manager.active_plugins.keys():
        print(f"loaded plugin: {plugin_name}")
    print("\n")

    # This plugin also supports plugin categories and allows you to get plugins from specified categories
    # For this add and subtract are both in the CalculatorPluginBase category.
    # This is driven by the plugin base they are using in this case all calculator operations use the
    # CalculatorPluginBase
    print("Showing Calculator Operation Plugins")
    operation_plugins = plugin_manager.categorized_plugins.get("CalculatorPluginBase")
    for plugin_name in operation_plugins.keys():
        print(f"loaded operations: {plugin_name}")
    print("\n")

    # Use the plugin name provided in the info file.
    # This will return a Plugin, Plugins contain the information provided in the info file and a plugin_object. The
    # 'plugin_object is the actual instantiated Object that contains the plugin's functionality.
    print("Showing Add functionality")
    add_plugin = plugin_manager.get_active_plugin("Add Plugin")
    add_operation = add_plugin.plugin_object
    value = add_operation.operation(1, 2)
    print(f"added 1 and 2: {value}")
    print("\n")

    print("Showing Subtract functionality")
    subtract_plugin = plugin_manager.get_active_plugin("Subtract Plugin")
    subtract_operation = subtract_plugin.plugin_object
    value = subtract_operation.operation(1, 2)
    print(f"subracted 1 and 2: {value}")
