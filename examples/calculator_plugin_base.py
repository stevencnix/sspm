from sspm.plugin_base import PluginBase
from abc import ABC, abstractmethod


class CalculatorPluginBase(ABC, PluginBase):

    @abstractmethod
    def operation(self, x, y):
        pass