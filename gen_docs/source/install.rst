Installation
============

The easiest way to install is to use pip:

	pip install SSPM

or if you have cloned the repo:

	cd <path to repo>
	pip install .

	or

	cd <path to repo>
	python setup.py install


Basic Usage
-----------

1. Initialize the plugin manager:

	``` shell
	plugin_manager = PluginManager(plugin_folder=\<INSERT PLUGINS DIR PATH HERE\>)
	```

2. Import the plugins in the plugins directory:

	``` shell
	plugin_manager.import_plugins()
	```

3. Get the imported plugin:

	```shell
	plugin = sspm.get_active_plugin("Plugin name")
	```

    or

    ```shell
    plugins = sspm.active_plugins
    ```