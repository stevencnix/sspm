sspm package
=============

.. automodule:: sspm
    :members:
    :undoc-members:
    :show-inheritance:
    :noindex:

Subpackages
-----------

.. toctree::
    :maxdepth: 4

Submodules
----------

.. toctree::
    :maxdepth: 4

    sspm.plugin
    sspm.plugin_base
    sspm.pluginmanager
