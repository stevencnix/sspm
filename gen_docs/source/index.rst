.. Super Simple Plugin Manager (SSPM) documentation master file, created by
   sphinx-quickstart on Mon Mar 13 17:46:42 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Super Simple Plugin Manager (SSPM)'s documentation!
==============================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   Introduction <readme.rst>
   Installation <install.rst>
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
